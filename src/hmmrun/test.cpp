

//**************************************
// FILE     : main.cpp
// AUTHOR(S): Avinesh PVS
//                 &
//            Ravikiran V
// PART OF  : HMM based Tagger
// CREATED  : 28th Feb, 2007
//
// MODIFIED LAST : 7th Sep 2007
//***************************************

/* This program takes train parameters (train.123 and train.lex)and test_file as arguments.
   It builds the model using the parameters generated in the training phase and then tags the sentences in
   the test_file with the best possible tag sequence using Viterbi Algorithm.
   This is a HMM based tagger.
   For the options please check --help. 
*/


#include "init.h"
#include "testing.c"
#include "stdlib.h"
#include<unistd.h>
#include<getopt.h>
static int help_flag;

#define MAX_TAGS 500

main(int argc,char *argv[])
{
        int i=0,j=0,z=0,l=0,svalue=5;
        char train[50];
        char num1[30],num2[30];

        char dchar[10];
        char nchar[10]="\0";
	
        FILE *fp1,*fp2,*tfile,*ofile;
	char ch;

	char *nvalue = NULL;
	char *dvalue = NULL;
	char *output = NULL;
	char *input_lex = NULL;
	char *input_123= NULL;
	char *test= NULL;
	int c;
	

	while (1)
	{
		static struct option long_options[] =
		{
			/* These options set a flag. */
			{"help", no_argument,       &help_flag, 1},
			/* These options don't set a flag.
			   We distinguish them by their indices. */
			{"ngram",     required_argument,       0, 'n'},
			{"smoothing", required_argument,       0, 'd'},
			{"train_para_lex",  required_argument, 0, 'b'},
			{"train_para_123",  required_argument, 0, 'c'},
			{"test",  required_argument, 0, 't'},
			{"output",    required_argument, 0, 'o'},
			{0, 0, 0, 0}
		};
		/* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long (argc, argv, "n:b:o:t:c:d:",
				long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c)
		{
			case 0:
				/* If this option set a flag, do nothing else now. */
				if (long_options[option_index].flag != 0)
					break;
				printf ("option %s", long_options[option_index].name);
				if (optarg)
					printf (" with arg %s", optarg);
				printf ("\n");
				break;

			case 'o':
				output=optarg;
				//printf ("option -o with value `%s'\n", optarg);
				break;
			case 'b':
				input_lex=optarg;
				//printf ("option -b with value `%s'\n", optarg);
				break;

			case 'c':
				input_123=optarg;
				//printf ("option -c with value `%s'\n", optarg);
				break;
			case 't':
				test=optarg;
				//printf ("option -c with value `%s'\n", optarg);
				break;

			case 'd':
				dvalue=optarg;
				if(dvalue==NULL)
				{
					printf("Error: Please specify the smoothing value\n");
					exit(0);
				}
				//printf ("option -d with value `%s'\n", optarg);
				break;

			case 'n':
				nvalue=optarg;
				if(nvalue==NULL)
				{
				 	printf("Error: Please specify the Ngram value\n");
					exit(0);
				}
				//printf ("option -n with value `%s'\n", optarg);
				break;

			case '?':
				/* getopt_long already printed an error message. */
				break;

			default:
				abort ();
		}
	}

	/* Instead of reporting `--'
	   and `--brief' as they are encountered,
	   we report the final status resulting from them. */
	if (help_flag)
	{
		printf("HMM Tagger Test - HMM Tagger Test version 1.0 (7th Sep 2007)\n");
		printf("usage: ./hmmrun [arguments] --train_para_lex=" " --train_para_123=" " --test=" " --output=" "\n");
		printf("\t Tagging the test file using train_para_lex and train_para_123 generated during training\n");
		printf("\nArguments:\n");
		printf("\t--ngram=value\t\tNgram Value (i.e trigram,bigram or unigram)\n");
		printf("\t--smoothing=value\tSmoothing Value\n");
		printf("\t\t\t\tvalue=1  - Addone\n\t\t\t\tvalue=2  - BackOff\n\t\t\t\tvalue=3  - WittenBell\n\t\t\t\tvalue=4  - GoodTuring\n\t\t\t\tvalue=5  - Deleted Interpolation\n\t\t\t\tvalue=5/0.3/0.2 - Deleted Interpolation with lamda values 0.3,0.2 and 0.5\n");
		exit(1);
	}

	if(nvalue!=NULL)
		order=atoi(nvalue);
	if(dvalue!=NULL)
	{
		strcpy(dchar,dvalue);
		if(dchar[0]=='1')
			svalue=1;	
		if(dchar[0]=='2')
			svalue=2;
		if(dchar[0]=='3')
			svalue=3;
		if(dchar[0]=='4')
			svalue=4;
		if(dchar[0]=='5')
		{
			svalue=5;
			if(dchar[1]=='/')
			{
				for(z=2;;z++)
				{
					if(dchar[z]!='/')
						num1[z-2]=dchar[z];
					if(dchar[z]=='\0')
					{	
						printf("Check the format for the options\n");
						printf("It should be: --smoothing=value/value1/value2\n");
						printf("\t\tvalue1 and value2 are normalized lamda values\n");
						exit(0);
					}				
					if(dchar[z]=='/')
					{	
						break;
					}
				}
				num1[z-2]='\0';
				for(l=z+1;;l++)
				{
					if(dchar[l]!='\0')
						num2[l-(z+1)]=dchar[l];
					else
						break;
				}

				num2[l-(z+1)]='\0';
				norm_lm1=atof(num1);
				norm_lm2=atof(num2);
				norm_lm3=1-norm_lm1-norm_lm2;
			}
		}
	}
	/*Training Corpus*/
	
	fp1 = fopen(input_123,"r");
	fp2 = fopen(input_lex,"r");
	if(fp1==NULL)
	{
		printf("Training-Corpus .123 does not exist\n");
		exit(0);
	}
	if(fp2==NULL)
	{
		printf("Training-Corpus .lex does not exist\n");
		exit(0);
	}


        trans=(int**)malloc(sizeof(int*)*100000);
        for(i=0;i<100000;i++)
        {
                trans[i]=(int*)malloc(sizeof(int)*100);
        }

	Trans_Matrix(fp1);
	fclose(fp1);
	/*BUILDING THE LETTER TREE (SUFFIX TRIE) AND FORMING THE TRANS MATRIX*/ 

        tree *ltree;
        ltree=(tree*)malloc(sizeof(tree));
        for(i=0;i<MAX_TAGS;i++)
                ltree->child[i]=NULL;
	/*Building the Suffix Tree LTREE and also calculating the prev_tag sequence and current_tag counts in TRANS matrix */
	
        ltree=LTree(ltree,fp2);
	fclose(fp2);

	//printf("hello2\n");
        /*Calculating the Lambda values NORM_LM1,NORM_LM2, NORM_LM3 for Deleted Interpolation Smoothing */     
	Train_Lambda();
	

	/*Test Corpus*/
	//printf("argv2- %s\n",argv[argc-1]);	


	tfile=fopen(test,"r");
	if(tfile==NULL)
	{
		printf("Test-Corpus does not exist\n");
		exit(0);
	}
	
	/*Assigning the best possible tag sequence to the sentences in the test file*/

        Tagging(tfile,output,ltree,svalue);   
	//printf("hello3\n");
        fclose(tfile);
}

