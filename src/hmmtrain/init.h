//Description
//* The program uses viterbi's algorithm to find the best tag sequence for a given input.
//* The input to the program is the name of the training corpus and the file that has to be tagged.
//* The output is the tagged file and is displayed on the standard output.
//* For further queries refer to the README*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<sys/time.h>

#include<map>
#include<iostream>
#include<fstream>

using namespace std;

using namespace __gnu_cxx;

map<string,int> map_ngram; // hash for ngram tag sequences 
map<string,int> map_tag;   // hash for tags
map<string,int> map_word;   // hash for words


int **trans; 	  // Transition Probability
int **word_tag_freq; 	  // Transition Probability
int ngramcnt;	
int tagc = 0;	  // Number of unique tags
int word_unique = 0;	  // Number of unique tags
int ngramc = 0;   // Tag sequence counts
int tokencount=0; //Size of the training corpus
int *Narray; 	  // counts of the number of occurences of the ngram tag sequences
int *uni; 	  // unigram counts of the tags 
float ** Ctrans;  // Changed Transition Matrix used for Good Turing Smoothing
char **words; 	  // Array of ngram tag sequences
char **ngram; 	  // Array of ngram tag sequences
char **tag; 	  // Array of Unigram Tags 
int sp_ind=0;     // Number of special characters 
int Wfreq=0; 		

//Description of the functions are described in func.c just above the function
void Training_Model(FILE *);
void Print_Train(char*,char *);
