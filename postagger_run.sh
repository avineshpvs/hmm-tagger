
# This perl program convert the SSF input to TNT foramt.
perl ssf2tnt_run.pl $1 > OUTPUT.tmp/ssf2tnt.tmp

# This HMM Run Engine exe to tagged the input file
bin/hmm_run --ngram=2 --smoothing=5 --train_para_lex=data_bin/hin/train.lex --train_para_123=data_bin/hin/train.123 --test=OUTPUT.tmp/ssf2tnt.tmp --output=OUTPUT.tmp/postagger.tnt

# This perl programm convert back intermediate output TNT foramt to SSF format
#perl tnt2ssf_run.pl OUTPUT.tmp/postagger.tnt

#cat postagger.out;
