PREFIX = $(PWD)
# list of programm files directories.
SUBDIRS = src

.PHONY: $(SUBDIRS)

all: $(SUBDIRS)

# compiles the source code as well as the data
$(SUBDIRS): 
	$(MAKE) -C $@ 
# make install -- Install what all needs to be installed, copying the files from the packages tree to systemwide directories.# it installs the engine and the corpus, dictionary, etc.
install:
	mkdir -p $(PREFIX)/bin/sl/postagger/common
	cp src/hmmtrain/hmm_train $(PREFIX)/bin/sl/postagger/common
	cp src/hmmtrain/hmm_train ./bin/
	cp src/hmmrun/hmm_run $(PREFIX)/bin/sl/postagger/common
	cp src/hmmrun/hmm_run ./bin/
	cp ssf2tnt_run.pl ssf2tnt_train.pl tnt2ssf_run.pl tnt2ssf_train.pl $(PREFIX)/bin/sl/postagger/common
	cp postagger_train.sh postagger_run.sh $(PREFIX)/bin/sl/postagger/common

uninstall:
	rm -fr $(PREFIX)/bin/sl/postagger/common
# make clean -- cleaning everything that can be automatically recreated with "make".
clean:
	$(MAKE) -C src clean
	rm -fr bin/hmm_train bin/hmm_run
#	rm -fr postagger.tmp postagger.tnt postagger.out
