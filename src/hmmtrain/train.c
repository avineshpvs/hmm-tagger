//**************************************
// FILE     : train.c
// AUTHOR(S): Avinesh PVS
//                 &
//            Ravikiran V
// PART OF  : HMM based Tagger
// CREATED  : 28th Feb 2007
// LAST MODIFIED : 31st May 2012
//***************************************

/* This program takes training_file and test_file as arguments.
   It builds the model using the training_file and then tags the sentences in the test_file with the best possible
   tag sequence using Viterbi Algorithm.
   This is a HMM based tagger.
   For the options please check the README in the Docs directory
*/


#include "init.h"
#include "train_func.c"
#include "stdlib.h"
#include<unistd.h>
#include<getopt.h>
static int help_flag;

int main(int argc,char *argv[])
{
        int i=0;
	char *train=NULL;
	char *train_123=NULL;
	char *train_lex=NULL;
	FILE *fp;
	int c;

	while (1)
	{
		static struct option long_options[] =
		{
			/* These options set a flag. */
			{"help", no_argument,       &help_flag, 1},
			/* These options don't set a flag. We distinguish them by their indices. */
			{"train",  required_argument, 0, 't'},
			{"output_123",  required_argument, 0, 'a'},
			{"output_lex",  required_argument, 0, 'b'},
			{0, 0, 0, 0}
		};
		/* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long (argc, argv, "t:",
				long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c)
		{
			case 0:
				/* If this option set a flag, do nothing else now. */
				if (long_options[option_index].flag != 0)
					break;
				int c;
			case 't':
				tagged_file = optarg;
				break;
			case 'a':
				train_123 = optarg;
				break;
			case 'b':
				train_lex = optarg;
				break;
			case '?':
				/* getopt_long already printed an error message. */
				break;
			default:
				abort ();

		}
	}
	if (help_flag)
	{
		printf("HMM Tagger Train - HMM Tagger Train version 1.0 (7th Sep 2007)\n");
		printf("usage:./train --train=train_file --output_123=" " --output_lex=" " \n");
		printf("\t\t This generates the\n");
		printf("\t\t output_lex   : Consists all lexical tag probablities\n");
		printf("\t\t output_123   : Conists of tag -> tag probabilities\n");
		printf("\t\t * default: train.123 and train.lex \n");
		exit(0);
	}	       
	/*Training Corpus*/
	fp = fopen(tagged_file,"r");
	
	if(fp == NULL)
	{
		sprintf("%s does not exist\n",tagged_file);
		exit(0);
	}
	if(train_123 == NULL)
	{
        train_123 = "train.123";
	}
	if(train_lex == NULL)
	{
        train_lex = "train.lex";
	}
    
    trans = (int**)malloc(sizeof(int*)*100000);
    for(i = 0; i<100000; i++)
    {
        trans[i] = (int*)malloc(sizeof(int)*100);
    }
	
    word_tag_freq=(int**)malloc(sizeof(int*)*100000);
    for(i = 0; i<100000; i++)
    {
        word_tag_freq[i] = (int*)malloc(sizeof(int)*100);
    }
    
    CreateModel(fp);
	
    fclose(fp);
	Print_Train(train_123,train_lex);
	exit(0);
}
