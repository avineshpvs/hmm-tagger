# This shell file used for training the Chunker
echo "Preparing the Training Parameter....";

# This perl programm convert SSF input to BIO format
perl ssf2tnt_train.pl $1 > data_src/hin/chunked_hin_datawx_final.tnt

# This HMM Train Engine exe is genarate the training parameters
bin/hmm_train --train=data_src/hin/chunked_hin_datawx_final.tnt --output_lex=data_bin/hin/train.lex --output_123=data_bin/hin/train.123
echo "Training Parameters are Created Successfully!....";
