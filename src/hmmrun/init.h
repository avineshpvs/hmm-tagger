//Description
//* The program uses viterbi's algorithm to find the best tag sequence for a given input.
//* The input to the program is the name of the training corpus and the file that has to be tagged.
//* The output is the tagged file and is displayed on the standard output.
//* For further queries refer to the README*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<sys/time.h>

#include<map>
#include<iostream>
#include<fstream>

using namespace std;

using namespace __gnu_cxx;

map<string,int> map_ngram; // hash for ngram tag sequences 
map<string,int> map_tag;   // hash for tags

typedef struct  tree
{
        int *wordfreq; //Word Frequencies
        int *suffreq;  // Suffix Frequencies
        struct tree* child[500]; 
}tree;

int **trans; 	  // Transition Probability
int **word_tag_freq; 	  // Transition Probability
int ngramcnt;	
int tagc = 0;	  // Number of unique tags
int word_unique = 0;	  // Number of unique tags
int order=2;	  // Order of the Ngram i.e 1- Unigrams, 2 - Bigrams, 3- Trigrams
int ngramc = 0;   // Tag sequence counts
int tokencount=0; //Size of the training corpus
int *Narray; 	  // counts of the number of occurences of the ngram tag sequences
int *uni; 	  // unigram counts of the tags 
float ** Ctrans;  // Changed Transition Matrix used for Good Turing Smoothing
char **words; 	  // Array of ngram tag sequences
char **ngram; 	  // Array of ngram tag sequences
char **tag; 	  // Array of Unigram Tags 

int addc=1; 	  // Constant value to be added in Addone Smoothing
float norm_lm1=0.0,norm_lm2=0.0,norm_lm3=0.0; // Normalized weights for Deleted Interpolation Smoothing 
char sp_char[100][2]; // Array of special characters dynamically built 
int sp_ind=0;     // Number of special characters 
int Wfreq=0; 		

//Description of the functions are described in func.c just above the function
int find_index(char c);
tree* build(tree* ltree,char *ch,int ind,int word_tag_freq);
int search(char *word,tree *ltree,int ind);
double emit_smooth(tree* ltree,char* presword,int prestag,int unigcnt);
tree* LTree(tree* ltree,FILE *fp);
float MAX_num(float a1,float a2,float a3);
void Train_Lambda();
float Deleted_Interpolation(char *tngram,int index);
float Backoff(char *tngram,int index);
void Cal_Narray(void); 
void Change_Trans();
float GoodTuring(char *tngram, int k);
float WittenBell(char *tngram, int k);
void  Tagging(FILE *tf,char *ofile,tree* ltree,int svalue);
void Trans_Matrix(FILE *fp);
