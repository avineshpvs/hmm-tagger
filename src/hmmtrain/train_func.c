/*This file consists of all the functions required during training of HMM Tagger*/

/* Written by Avinesh PVS, avinesh@students.iiit.ac.in
   and V. Ravi Kiran, ravikiranv@students.iiit.ac.in */	

//Training
void CreateModel(FILE *fp)
{
	int i,j,k=0,n=0;
	char ptag[50],*ch;

	ch=(char*)malloc(sizeof(char)*100);
	string str_ptag;
	string str_ch;
	char *prev1;
	char *prev2;
	char *sent;
	char *temp,*temp_sent,*uni_temp;

	words=(char **)malloc(sizeof(char*)*100000);
	for(i=0;i<100000;i++)
		words[i]=(char *)malloc(50* sizeof(char));
	
	
	ngram=(char **)malloc(sizeof(char*)*100000);
	for(i=0;i<100000;i++)
		ngram[i]=(char *)malloc(50 * sizeof(char));

	tag=(char **)malloc(sizeof(char*)*1000);
	for(i=0;i<1000;i++)
		tag[i] = (char *)malloc(50 * sizeof(char));

	uni=(int*)malloc(sizeof(int)*1000);
	for(i=0;i<1000;i++)
		uni[i]=0;

	prev1=(char*)malloc(sizeof(char)* 100);
	sent=(char*)malloc(sizeof(char)* 100);
	prev2=(char*)malloc(sizeof(char)* 100);
	temp=(char*)malloc(sizeof(char)* 100);
	temp_sent=(char*)malloc(sizeof(char)* 100);
	uni_temp=(char*)malloc(sizeof(char)* 100);

	strcpy(prev1,"START:");
	strcpy(prev2,"START:");
	string temp1;
	
	while(fscanf(fp,"%[^\n]",sent)!=EOF)
	{
		fgetc(fp);
		strcpy(temp_sent,sent);
		ch = (char *)strtok(temp_sent," ");
		if(ch!=NULL)
		{
			sscanf(sent,"%s %s",ch,ptag);
			str_ptag=string(ptag);
			i=map_tag[str_ptag];
			i=i-1;
			if(i==-1)
			{
				strcpy(tag[tagc],ptag);
				map_tag[str_ptag]=tagc+1;
				tagc++;

				i=map_tag[str_ptag];
				i=i-1;
			}

			uni[i]=uni[i]+1;

			str_ch=string(ch);
			k=map_word[str_ch];
			k=k-1;
			if(k==-1)
			{
			    strcpy(words[word_unique],ch);
			    map_word[str_ch]=word_unique+1;
			    word_unique++;
			    k=map_word[str_ch];
			    k=k-1;
			}	
			word_tag_freq[k][i]=word_tag_freq[k][i]+1;

			
			tokencount=tokencount+1;
			strcpy(temp,prev2);
			strcat(temp,prev1);

			strcpy(uni_temp,prev1);


			temp1=string(temp);
			j=map_ngram[temp1];
			j=j-1;
			if(j==-1)
			{

				strcpy(ngram[ngramc],temp);
				map_ngram[temp1]=ngramc+1;

				ngramc++;
				j=map_ngram[temp1];
				j=j-1;
			}
			trans[j][i]=trans[j][i]+1;


			temp1=string(uni_temp);
			j=map_ngram[temp1];
			j=j-1;
			if(j==-1)
			{

				strcpy(ngram[ngramc],uni_temp);
				map_ngram[temp1]=ngramc+1;

				ngramc++;
				j=map_ngram[temp1];
				j=j-1;
			}
			trans[j][i]=trans[j][i]+1;



			strcpy(prev2,prev1);
			strcpy(prev1,ptag);
			strcat(prev1,":");
		}
		else
		{	
			strcpy(prev2,"START:");
			strcpy(prev1,"START:");
		}
		strcpy(temp_sent,"\0");
		strcpy(sent,"\0");

	}
}


void Print_Train(char *train_123,char *train_lex)
{
	FILE *fp1;
	FILE *fp2;

	int j=0;
	int i=0;
	
	fp1=fopen(train_123,"w");
	fprintf(fp1,"%% %s\n",train_123);
	fprintf(fp1,"%% the corpus contained %d words\n",tokencount);
	fprintf(fp1,"%% the corpus contained %d tags and %d ngrams\n",tagc,ngramc);

	char tmp1[50],tmp2[50],tmp3[50];
	string str_tmp1;
	string str_tmp2;
	string str_tmp3;
	for(int i=0;i<ngramc;i++)
	{
		tmp1[0]='\0';
		tmp2[0]='\0';
		int k=0;
		while(ngram[i][k]!=':')
		{
			tmp1[k]=ngram[i][k];
			tmp2[k]=ngram[i][k];
			k++;
		}
		
		tmp1[k]=':';
		tmp1[k+1]='\0';
		tmp2[k]='\0';
		
		str_tmp2=string(tmp2);
		int m=0;
		m=map_tag[str_tmp2];
		m=m-1;
		
		int l=0;
	
		k++;
		if(ngram[i][k]!='\0')
		{
			while(ngram[i][k]!=':')
			{
				tmp3[l]=ngram[i][k];
				k++;
				l++;
			}
			tmp3[l]='\0';
			
		}
		else
		{
			continue;
		}


		fprintf(fp1,"%s %d\n",tmp2,uni[m]);
		int n=0,p=0;
		str_tmp1=string(tmp1);
		str_tmp3=string(tmp3);
		n=map_ngram[str_tmp1];
		n=n-1;
		p=map_tag[str_tmp3];
		p=p-1;
		
		fprintf(fp1,"\t%s %d\n",tmp3,trans[n][p]);
		for(int j=0;j<tagc;j++)
		{
			if(trans[i][j]!=0)
			{
				fprintf(fp1,"\t\t%s %d\n",tag[j],trans[i][j]);		
			}
		}
	}
	fclose(fp1);
	
	
	fp2=fopen(train_lex,"w");

	fprintf(fp2,"%% %s\n",train_lex);
	fprintf(fp2,"%% the corpus contained %d tokens\n",tokencount);
	fprintf(fp2,"%% this lexicon contains %d tokens\n",word_unique);
	for(int i=0;i<word_unique;i++)
	{
		fprintf(fp2,"%s",words[i]);
		for(int j=0;j<tagc;j++)
		{
			if(word_tag_freq[i][j]!=0)
			fprintf(fp2," %s %d",tag[j],word_tag_freq[i][j]);
		}
		fprintf(fp2,"\n");
	}
	fclose(fp2);
	
}
